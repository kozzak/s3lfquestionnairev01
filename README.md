# S3LFQuestionnaireV01

Primera versión del cuestionario diseñado para medir el nivel de autodeterminación de cada una de las personas que acaba de participar de un proceso de toma de decisiones colaborativo basado en el consentimiento o el consenso.